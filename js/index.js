const visorInyematic=new Vue({
    el:'#visorInyematic',
    data: {
        imagenes:['/img/Inyematic/cuentas.png','/img/Inyematic/maquinas.png','/img/Inyematic/BolsasMaterial.png','/img/Inyematic/clientes.png','/img/Inyematic/grafica.png','/img/Inyematic/Ilogin2.png','/img/Inyematic/Iordenes3.png','/img/Inyematic/moldes.png','/img/Inyematic/operadores.png','/img/Inyematic/VerOrden.png'],
        posicion:0
    },
    methods:{
        adelante(){
         
            let tam=this.imagenes.length;
         
            if( this.posicion == tam-1)
                this.posicion = 0;
            else
                this.posicion++;
            
            this.imagen=this.imagenes[this.posicion];
        },
        atras(){
          
            let tam=this.imagenes.length;

            if(this.posicion == 0)
                this.posicion = tam-1;
            else
                this.posicion--;

            this.imagen=this.imagenes[this.posicion];
        }

    },
    computed:{
        imagen(){
            return this.imagenes[this.posicion];
        }
    }
});

const visorLibreta=new Vue({
    el:'#visorLibreta',
    data: {
        imagenes:['../img/LibretaSuper/img1.png','../img/LibretaSuper/img2.png','../img/LibretaSuper/img3.png','../img/LibretaSuper/img4.png','../img/LibretaSuper/img5.png'],
        posicion:0
    },
    methods:{
        adelante(){
         
            let tam=this.imagenes.length;
         
            if( this.posicion == tam-1)
                this.posicion = 0;
            else
                this.posicion++;
            
            this.imagen=this.imagenes[this.posicion];
        },
        atras(){
          
            let tam=this.imagenes.length;

            if(this.posicion == 0)
                this.posicion = tam-1;
            else
                this.posicion--;

            this.imagen=this.imagenes[this.posicion];
        }

    },
    computed:{
        imagen(){
            return this.imagenes[this.posicion];
        }
    }
});